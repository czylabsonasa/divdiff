// the method of divided differences 
// 0. version
#include <cstdio>
#include <vector>
using std::vector;
typedef vector<double> dvec;
void olvas(dvec& x){
   for(size_t i=0;i<x.size();i++){
      scanf("%lf",&x[i]);
   }
}
void ir(const dvec& x){
   for(size_t i=0;i<x.size();i++){
      printf("%lf ",x[i]);
   }
   printf("\n");
}
double genhorner(const dvec& p,const dvec& t,double x){
   double px=0;
   for(size_t i=0;i<p.size();i++){
      px=px*(x-t[i])+p[i];
   }
   return px;
}

int main(){
   // reading the coordinates of the points
   int n;scanf("%d",&n);
   dvec t(n);
   vector<dvec> f(n);
   f[0].resize(n);
   olvas(t); 
   olvas(f[0]);
   // fill in the table
   for(int i=1;i<n;i++){
      f[i].resize(n-i);
      for(int j=0;j<n-i;j++){
         f[i][j]=(f[i-1][j]-f[i-1][j+1])/(t[j]-t[j+i]);
      }
   }
   dvec p(n);//the polinom from the "right" hand side of the table
   for(int i=0;i<n;i++){
      p[i]=f[n-1-i][i];
   }
   // evaluate the poly at some points:
   double x;
   while(1==scanf("%lf",&x)){
      printf("\tp(%lf) = %lf\n",x,genhorner(p,t,x));
   }
   return 0;
}
